#include "threatcheck.as"

void SetParameters() {
    params.AddString("music", "atense");
}

void Init() {
    if(params.GetString("music") == "atense"){
        AddMusic("Data/Custom/timbles/therium-2/Music/atense.xml");
		PlaySong("ambient-tense");
    } else if(params.GetString("music") == "challengelevel"){
        AddMusic("Data/Music/challengelevel.xml");
		PlaySong("ambient-tense");
    } else if(params.GetString("music") == "b4b"){
        AddMusic("Data/Custom/timbles/therium-2/Music/b4b.xml");
    } else if(params.GetString("music") == "b4b2"){
        AddMusic("Data/Custom/timbles/therium-2/Music/b4b2.xml");
    } else if(params.GetString("music") == "bancients"){
        AddMusic("Data/Custom/timbles/therium-2/Music/bancients.xml");
    } else if(params.GetString("music") == "bboss"){
        AddMusic("Data/Custom/timbles/therium-2/Music/bboss.xml");
    } else if(params.GetString("music") == "bending"){
        AddMusic("Data/Custom/timbles/therium-2/Music/bending.xml");
    } else if(params.GetString("music") == "bfending"){
        AddMusic("Data/Custom/timbles/therium-2/Music/bfending.xml");
    } else if(params.GetString("music") == "b-s2"){
        AddMusic("Data/Custom/timbles/therium-2/Music/b-s2.xml");
    } else if(params.GetString("music") == "e-a4-a"){
        AddMusic("Data/Custom/timbles/therium-2/Music/e-a4-a.xml");
    } else if(params.GetString("music") == "e-a4-b"){
        AddMusic("Data/Custom/timbles/therium-2/Music/e-a4-b.xml");
    } else if(params.GetString("music") == "e-b3"){
        AddMusic("Data/Custom/timbles/therium-2/Music/e-b3.xml");
    } else if(params.GetString("music") == "hub"){
        AddMusic("Data/Custom/timbles/therium-2/Music/hub.xml");
    } else if(params.GetString("music") == "prologue"){
        AddMusic("Data/Custom/timbles/therium-2/Music/prologue.xml");
    } else if(params.GetString("music") == "reaper"){
        AddMusic("Data/Custom/timbles/therium-2/Music/reaper.xml");
    } else if(params.GetString("music") == "S2A3"){
        AddMusic("Data/Custom/timbles/therium-2/Music/S2A3.xml");
    } else if(params.GetString("music") == "spiritual"){
        AddMusic("Data/Custom/timbles/therium-2/Music/spiritual.xml");
	}
}

void Dispose() {
}


float blackout_amount = 0.0;
float ko_time = -1.0;

void Update() {
    int player_id = GetPlayerCharacterID();
    if(player_id != -1 && ReadCharacter(player_id).GetIntVar("knocked_out") != _awake){
        PlaySong("sad");
        return;
    }
    int threats_remaining = ThreatsRemaining();
    if(threats_remaining == 0){
        PlaySong("ambient-happy");
        return;
    }
    if(player_id != -1 && ReadCharacter(player_id).QueryIntFunction("int CombatSong()") == 1){
        PlaySong("combat");
        return;
    }
    PlaySong("ambient-tense");

	blackout_amount = 0.0;
	if(player_id != -1 && ObjectExists(player_id)){
		MovementObject@ char = ReadCharacter(player_id);
		if(char.GetIntVar("knocked_out") != _awake){
			if(ko_time == -1.0f){
				ko_time = the_time;
			}
			if(ko_time < the_time - 1.0){
				if(GetInputPressed(0, "attack") || ko_time < the_time - 5.0){
	            	level.SendMessage("reset"); 				                
				}
			}
            blackout_amount = 0.2 + 0.6 * (1.0 - pow(0.5, (the_time - ko_time)));
		} else {
			ko_time = -1.0f;
		}
	} else {
        ko_time = -1.0f;
    }
}

void PreDraw(float curr_game_time) {
    camera.SetTint(camera.GetTint() * (1.0 - blackout_amount));
}

void Draw(){
    if(EditorModeActive()){
        Object@ obj = ReadObjectFromID(hotspot.GetID());
        DebugDrawBillboard("Data/Custom/timbles/therium-2/Textures/logo256.png",
                           obj.GetTranslation(),
                           obj.GetScale()[1]*2.0,
                           vec4(vec3(0.5), 1.0),
                           _delete_on_draw);
    }
}
